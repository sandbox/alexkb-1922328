<?php

/**
 * Implements hook_action_rules_action_info().
 */
function intpath_relationships_rules_action_info() {
  return array(
    'intpath_relationships_action_record_links' => array(
      'label' => t("Record Inline links used in body"),
      'arguments' => array(
        'node' => array('type' => 'node', 'label' => t('Content'))
      ),
      'group' => t('Node'),
      'module' => 'intpath_relationships',
    )
  );
}

/**
 * Action for recording internal links
 * @param type $node
 * @return type
 */
function intpath_relationships_action_record_links($node) {
  $library = libraries_load('simplehtmldom');
  
  if ($library['installed']) {
    // @todo detect all fields that are rich text.
    if (isset($node->body[LANGUAGE_NONE][0]['value'])) {
      // delete all the links for this node first
      db_delete('intpath_relationships')->condition('parent_nid', $node->nid, '=')->execute();
      
      // determine what links are specified
      $inline_nids = _intpath_relationships_detect_inline_links($node->body[LANGUAGE_NONE][0]['value']);
            
      if (count($inline_nids) == 0) {
        return;
      }
      
      $insert = db_insert('intpath_relationships')->fields(array('parent_nid', 'child_nid', 'instances'));
      foreach($inline_nids as $nid => $instances) {
        if ($node->nid != $nid) { // incase its a self refering link.
          $insert->values(array('parent_nid' => $node->nid, 'child_nid' => $nid, 'instances' => $instances));
        }
      }
      $insert->execute();
    }
  }
}
